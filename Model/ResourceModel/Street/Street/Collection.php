<?php
declare(strict_types=1);

namespace Perspective\NovaposhtaCatalog\Model\ResourceModel\Street\Street;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Perspective\NovaposhtaCatalog\Model\ResourceModel\Street\Street as StreetResource;
use Perspective\NovaposhtaCatalog\Model\Street\Street;

class Collection extends AbstractCollection
{
    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        $this->_init(Street::class, StreetResource::class);
    }

    /**
     * Filter collection by City Ref
     *
     * @param string $cityRef
     * @return Collection<Street>
     */
    public function addCityRefToFilter(string $cityRef): Collection
    {
        return $this->addFieldToFilter(\Perspective\NovaposhtaCatalog\Api\Data\StreetInterface::CITY_REF, $cityRef);
    }
}
