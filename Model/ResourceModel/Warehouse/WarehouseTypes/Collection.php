<?php


namespace Perspective\NovaposhtaCatalog\Model\ResourceModel\Warehouse\WarehouseTypes;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    protected function _construct()
    {
        $this->_init(
            \Perspective\NovaposhtaCatalog\Model\Warehouse\WarehouseTypes::class,
            \Perspective\NovaposhtaCatalog\Model\ResourceModel\Warehouse\WarehouseTypes::class
        );
    }
}
