<?php


namespace Perspective\NovaposhtaCatalog\Model\Update;

use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\HTTP\ZendClientFactory;
use Magento\Framework\Serialize\Serializer\Json;
use Perspective\NovaposhtaCatalog\Helper\Config;
use Perspective\NovaposhtaCatalog\Helper\CronSyncDateLastUpdate;
use Perspective\NovaposhtaCatalog\Model\ResourceModel\Warehouse\Warehouse\Collection;
use Perspective\NovaposhtaCatalog\Model\ResourceModel\Warehouse\Warehouse\CollectionFactory;
use Perspective\NovaposhtaCatalog\Model\Warehouse\WarehouseFactory;

class Warehouse
{
    /**
     * @var \Magento\Framework\HTTP\ZendClientFactory
     */
    protected $httpClientFactory;

    /**
     * @var \Perspective\NovaposhtaCatalog\Helper\Config
     */
    protected $configHelper;

    /**
     * @var \Perspective\NovaposhtaCatalog\Model\Warehouse\WarehouseFactory
     */
    protected $warehouseFactory;

    /**
     * @var \Perspective\NovaposhtaCatalog\Model\ResourceModel\Warehouse\Warehouse
     */
    protected $warehouseResourceModel;

    /**
     * @var \Perspective\NovaposhtaCatalog\Model\ResourceModel\Warehouse\Warehouse\Collection
     */
    protected $warehouseCollectionResourceModel;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var \Perspective\NovaposhtaCatalog\Model\ResourceModel\Warehouse\Warehouse\CollectionFactory
     */
    protected $warehouseResourceModelCollectionFactory;

    /**
     * @var \Magento\Framework\Serialize\Serializer\Json
     */
    private $jsonSerializer;

    /**
     * @var \Perspective\NovaposhtaCatalog\Helper\CronSyncDateLastUpdate
     */
    private $cronSyncDateLastUpdate;

    /**
     * Warehouse constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory
     * @param \Perspective\NovaposhtaCatalog\Helper\Config $configHelper
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Perspective\NovaposhtaCatalog\Helper\CronSyncDateLastUpdate $cronSyncDateLastUpdate
     * @param \Magento\Framework\Serialize\Serializer\Json $jsonSerializer
     * @param \Perspective\NovaposhtaCatalog\Model\Warehouse\WarehouseFactory $warehouseFactory
     * @param \Perspective\NovaposhtaCatalog\Model\ResourceModel\Warehouse\Warehouse $warehouseResourceModel
     * @param Collection $warehouseCollectionResourceModel
     * @param CollectionFactory $warehouseResourceModelCollectionFactory
     */
    public function __construct(
        ZendClientFactory $httpClientFactory,
        Config $configHelper,
        CronSyncDateLastUpdate $cronSyncDateLastUpdate,
        Json $jsonSerializer,
        WarehouseFactory $warehouseFactory,
        \Perspective\NovaposhtaCatalog\Model\ResourceModel\Warehouse\Warehouse $warehouseResourceModel,
        Collection $warehouseCollectionResourceModel,
        CollectionFactory $warehouseResourceModelCollectionFactory
    ) {
        $this->warehouseResourceModelCollectionFactory = $warehouseResourceModelCollectionFactory;
        $this->warehouseCollectionResourceModel = $warehouseCollectionResourceModel;
        $this->warehouseResourceModel = $warehouseResourceModel;
        $this->warehouseFactory = $warehouseFactory;
        $this->httpClientFactory = $httpClientFactory;
        $this->configHelper = $configHelper;
        $this->jsonSerializer = $jsonSerializer;
        $this->cronSyncDateLastUpdate = $cronSyncDateLastUpdate;
    }

    /**
     * @inheritDoc
     * @throws \Zend_Http_Client_Exception
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        $message = "Error has been occur";
        $error = true;
        $data = [];
        if ($this->configHelper->getIsEnabledConfig()) {

            $warehouseListJsonEncoded = $this->getWarehouseListFromApiEndpoint();
            $warehouseListJsonDecoded = json_decode($warehouseListJsonEncoded);
            if (property_exists($warehouseListJsonDecoded, 'success')
                && $warehouseListJsonDecoded->success === true) {
                try {
                    $error = false;
                    $message = 'In Progress..';
                    $this->setWareHouseDataToDB($warehouseListJsonDecoded->data);
                } catch (AlreadyExistsException $e) {
                    $error = true;
                    $message = "Key already exist\n" . $e->getMessage();
                }
                if (!$error) {
                    $error = false;
                    $message = "Successfully synced";
                    $this->cronSyncDateLastUpdate
                        ->updateSyncDate($this->cronSyncDateLastUpdate::XML_PATH_LAST_SYNC_WAREHOUSE);
                }
            }
        }
        return [
            'message' => $message,
            'data' => $data,
            'error' => $error
        ];
    }

    /**
     * @param array $data
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function setWareHouseDataToDB(array $data)
    {
        $entireTableColl = $this->warehouseResourceModelCollectionFactory->create();
        $entireIds = $entireTableColl->getAllIds();
        foreach ($data as $idx => $datum) {
            $filledModel = $this->prepareData($datum);
            $singleItem = $this->warehouseFactory->create();
            $this->warehouseResourceModel->load($singleItem, $filledModel->getRef(), $filledModel::REF);
            if (($singleItem->getRef())) {
                $filledModel->setId($singleItem->getId());
                $this->warehouseResourceModel->save($filledModel);
                unset($entireIds[array_search($singleItem->getId(), $entireIds)]);
            } else {
                $this->warehouseResourceModel->save($filledModel);
            }
        }
        if (count($entireIds) > 0) {
            foreach ($entireIds as $remIdx => $remItem) {
                $cleanUpModel = $this->warehouseFactory->create();
                $this->warehouseResourceModel->load($cleanUpModel, $remItem, 'id');
                $this->warehouseResourceModel->delete($cleanUpModel);
            }
        }
    }

    /**
     * @param $datum
     * @return \Perspective\NovaposhtaCatalog\Model\Warehouse\Warehouse
     */
    public function prepareData($datum)
    {
        /**@var $warehouseModel \Perspective\NovaposhtaCatalog\Model\Warehouse\Warehouse */
        $warehouseModel = $this->warehouseFactory->create();
        isset($datum->SiteKey) ? $warehouseModel->setSiteKey($datum->SiteKey) : null;
        isset($datum->Description) ? $warehouseModel->setDescriptionUa($datum->Description) : null;
        isset($datum->DescriptionRu) ? $warehouseModel->setDescriptionRu($datum->DescriptionRu) : null;
        isset($datum->ShortAddress) ? $warehouseModel->setShortAddressUa($datum->ShortAddress) : null;
        isset($datum->ShortAddressRu) ? $warehouseModel->setShortAddressRu($datum->ShortAddressRu) : null;
        isset($datum->Phone) ? $warehouseModel->setPhone($datum->Phone) : null;
        isset($datum->TypeOfWarehouse) ? $warehouseModel->setTypeOfWarehouse($datum->TypeOfWarehouse) : null;
        isset($datum->Ref) ? $warehouseModel->setRef($datum->Ref) : null;
        isset($datum->Number) ? $warehouseModel->setNumberInCity($datum->Number) : null;
        isset($datum->CityRef) ? $warehouseModel->setCityRef($datum->CityRef) : null;
        isset($datum->CityDescription) ? $warehouseModel->setCityDescriptionUa($datum->CityDescription) : null;
        isset($datum->CityDescriptionRu) ? $warehouseModel->setCityDescriptionRu($datum->CityDescriptionRu) : null;
        isset($datum->SettlementRef) ? $warehouseModel->setSettlementRef($datum->SettlementRef) : null;
        isset($datum->SettlementDescription)
            ? $warehouseModel->setSettlementDescription($datum->SettlementDescription)
            : null;
        isset($datum->SettlementAreaDescription)
            ? $warehouseModel->setSettlementAreaDescription($datum->SettlementAreaDescription)
            : null;
        isset($datum->SettlementRegionsDescription)
            ? $warehouseModel->setSettlementRegionDescription($datum->SettlementRegionsDescription)
            : null;
        isset($datum->SettlementTypeDescription)
            ? $warehouseModel->setSettlementTypeDescription($datum->SettlementTypeDescription)
            : null;
        isset($datum->Longitude) ? $warehouseModel->setLongitude($datum->Longitude) : null;
        isset($datum->Latitude) ? $warehouseModel->setLatitude($datum->Latitude) : null;
        isset($datum->PostFinance) ? $warehouseModel->setPostFinance($datum->PostFinance) : null;
        isset($datum->BicycleParking) ? $warehouseModel->setBicycleParking($datum->BicycleParking) : null;
        isset($datum->PaymentAccess) ? $warehouseModel->setPaymentAccess($datum->PaymentAccess) : null;
        isset($datum->POSTerminal) ? $warehouseModel->setPOSTerminal($datum->POSTerminal) : null;
        isset($datum->InternationalShipping)
            ? $warehouseModel->setInternationalShipping($datum->InternationalShipping)
            : null;
        isset($datum->TotalMaxWeightAllowed)
            ? $warehouseModel->setTotalMaxWeightAllowed($datum->TotalMaxWeightAllowed)
            : null;
        isset($datum->PlaceMaxWeightAllowed)
            ? $warehouseModel->setPlaceMaxWeightAllowed($datum->PlaceMaxWeightAllowed)
            : null;
        isset($datum->Reception) ? $warehouseModel->setReception($datum->Reception) : null;
        isset($datum->Delivery) ? $warehouseModel->setDelivery($datum->Delivery) : null;
        isset($datum->Schedule) ? $warehouseModel->setSchedule($datum->Schedule) : null;
        isset($datum->DistrictCode) ? $warehouseModel->setDistrictCode($datum->DistrictCode) : null;
        isset($datum->WarehouseStatus) ? $warehouseModel->setWarehouseStatus($datum->WarehouseStatus) : null;
        isset($datum->CategoryOfWarehouse)
            ? $warehouseModel->setCategoryOfWarehouse($datum->CategoryOfWarehouse)
            : null;
        return $warehouseModel;
    }

    /**
     * @return string
     * @throws \Zend_Http_Client_Exception
     */
    protected function getWarehouseListFromApiEndpoint()
    {
        $apiKey = $this->configHelper->getApiKeyConfig();
        $request = $this->httpClientFactory->create();
        $request->setUri('https://api.novaposhta.ua/v2.0/json/AddressGeneral/getWarehouses');
        $params = ['modelName' => 'AddressGeneral', 'calledMethod' => 'getWarehouses', 'apiKey' => $apiKey];
        $request->setConfig(['maxredirects' => 0, 'timeout' => 60]);
        $request->setRawData(utf8_encode($this->jsonSerializer->serialize($params)));
        return $request->request(\Zend_Http_Client::POST)->getBody();
    }
}
