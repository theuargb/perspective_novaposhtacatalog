<?php
declare(strict_types=1);

namespace Perspective\NovaposhtaCatalog\Model\Update;

use Magento\Framework\HTTP\ZendClientFactory;
use Magento\Framework\Serialize\Serializer\Json;
use Perspective\NovaposhtaCatalog\Api\Data\CityInterface;
use Perspective\NovaposhtaCatalog\Api\Data\StreetInterface;
use Perspective\NovaposhtaCatalog\Api\Data\StreetInterfaceFactory;
use Perspective\NovaposhtaCatalog\Helper\Config;
use Perspective\NovaposhtaCatalog\Model\ResourceModel\Street\Street\CollectionFactory as StreetCollectionFactory;
use Perspective\NovaposhtaCatalog\Model\ResourceModel\City\City\CollectionFactory as CityCollectionFactory;

/**
 * Import Streets from NovaPoshta API
 *
 */
class Street
{
    /**
     * Field names in NP API response
     */
    private const API_FIELD_REF = 'Ref';

    private const API_FIELD_DESCRIPTION = 'Description';

    private const API_FIELD_STREETS_TYPE_REF = 'StreetsTypeRef';

    private const API_FIELD_STREETS_TYPE = 'StreetsType';

    /**
     * @var \Perspective\NovaposhtaCatalog\Model\ResourceModel\City\City\CollectionFactory
     */
    private $cityCollectionFactory;

    /**
     * @var \Perspective\NovaposhtaCatalog\Model\ResourceModel\Street\Street\CollectionFactory
     */
    private $streetCollectionFactory;

    /**
     * @var \Perspective\NovaposhtaCatalog\Api\Data\StreetInterfaceFactory
     */
    private $streetFactory;

    /**
     * @var \Perspective\NovaposhtaCatalog\Model\ResourceModel\Street\Street
     */
    private $streetResource;

    /**
     * @var \Perspective\NovaposhtaCatalog\Helper\Config
     */
    private $configHelper;

    /**
     * @var \Magento\Framework\HTTP\ZendClientFactory
     */
    private $httpClientFactory;

    /**
     * @var \Magento\Framework\Serialize\Serializer\Json
     */
    private $jsonSerializer;

    /**
     * @param \Perspective\NovaposhtaCatalog\Model\ResourceModel\City\City\CollectionFactory $cityCollectionFactory
     * @param \Perspective\NovaposhtaCatalog\Model\ResourceModel\Street\Street\CollectionFactory $streetCollectionFactory
     * @param \Perspective\NovaposhtaCatalog\Api\Data\StreetInterfaceFactory $streetFactory
     * @param \Perspective\NovaposhtaCatalog\Model\ResourceModel\Street\Street $streetResource
     * @param \Perspective\NovaposhtaCatalog\Helper\Config $configHelper
     * @param \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory
     * @param \Magento\Framework\Serialize\Serializer\Json $jsonSerializer
     */
    public function __construct(
        CityCollectionFactory $cityCollectionFactory,
        StreetCollectionFactory $streetCollectionFactory,
        StreetInterfaceFactory $streetFactory,
        \Perspective\NovaposhtaCatalog\Model\ResourceModel\Street\Street $streetResource,
        Config $configHelper,
        ZendClientFactory $httpClientFactory,
        Json $jsonSerializer
    ) {

        $this->cityCollectionFactory = $cityCollectionFactory;
        $this->streetCollectionFactory = $streetCollectionFactory;
        $this->streetFactory = $streetFactory;
        $this->streetResource = $streetResource;
        $this->configHelper = $configHelper;
        $this->httpClientFactory = $httpClientFactory;
        $this->jsonSerializer = $jsonSerializer;
    }

    /**
     * @param \Closure|null $cl
     */
    public function execute(\Closure $cl = null)
    {
        $message = "Error has been occur";
        $error = true;
        $data = [];
        $this->logger = $cl;

        foreach ($this->getCityCollection() as $city) {
            if ($city->getRef() === null) {
                continue;
            }
            $this->log('Importing for city ' . $city->getRef());
            $this->processStreetsForCityRef($city->getRef());
            $error = false;
            $message = "Successfully synced";
        }
        return [
            'message' => $message,
            'data' => $data,
            'error' => $error
        ];
    }

    /**
     * @return \Perspective\NovaposhtaCatalog\Model\ResourceModel\City\City\Collection<\Perspective\NovaposhtaCatalog\Model\City\City>
     */
    private function getCityCollection(): \Perspective\NovaposhtaCatalog\Model\ResourceModel\City\City\Collection
    {
        return $this->cityCollectionFactory
            ->create()
            ->removeAllFieldsFromSelect()
            ->addFieldToSelect(CityInterface::REF);
    }

    /**
     * @param string $cityRef
     */
    private function processStreetsForCityRef(string $cityRef): void
    {
        $streetsFromNP = $this->importStreets($cityRef);
        if (empty($streetsFromNP)) {
            $this->log(__('Street Request error. City has no Streets OR Check API key.'));
            return;
        }

        $streets = $this->getStreetsFromDb($cityRef);

        foreach ($streetsFromNP as $streetFromNP) {
            /** @var StreetInterface|null $street */
            $street = $streets->getItemByColumnValue(StreetInterface::REF, $streetFromNP->getRef());
            if ($street === null) {
                $this->saveStreet($streetFromNP);
            } else {
                if ($this->isStreetChanged($street, $streetFromNP)) {
                    $streetId = $street->getStreetId();
                    $this->saveStreet($streetFromNP, $streetId);
                }
            }
        }
    }

    /**
     * @param string $cityRef
     * @return array<StreetInterface>
     */
    private function importStreets(string $cityRef): array
    {
        $params = $this->prepareRequestParams($cityRef);

        $streetsData = [];
        try {
            $dataFromEndpoint = $this->jsonSerializer->unserialize($this->getCitiesListFromApiEndpoint($params));
        } catch (\InvalidArgumentException $invalidArgumentException) {
            return $streetsData;
        }

        $data = $dataFromEndpoint['data'] ?: null;
        if (!$data) {
            return $streetsData;
        }

        foreach ($data as $datum) {
            /** @var StreetInterface $street */
            $street = $this->streetFactory->create();
            $street->setRef($datum[self::API_FIELD_REF])
                ->setDescription($datum[self::API_FIELD_DESCRIPTION])
                ->setStreetTypeRef($datum[self::API_FIELD_STREETS_TYPE_REF])
                ->setStreetType($datum[self::API_FIELD_STREETS_TYPE])
                ->setCityRef($cityRef);
            $streetsData[] = $street;
        }

        return $streetsData;
    }

    /**
     * @return string
     * @throws \Zend_Http_Client_Exception
     */
    protected function getCitiesListFromApiEndpoint($params)
    {
        $apiKey = $this->configHelper->getApiKeyConfig();
        $request = $this->httpClientFactory->create();
        $request->setUri('https://api.novaposhta.ua/v2.0/json/Address/getCities');
        $params['apiKey'] = $apiKey;
        $request->setConfig(['maxredirects' => 0, 'timeout' => 60]);
//        $request->setRawData(utf8_encode(json_encode($params)));
        $request->setRawData(utf8_encode($this->jsonSerializer->serialize($params)));
        return $request->request(\Zend_Http_Client::POST)->getBody();
    }

    /**
     * @param string $cityRef
     * @return array<mixed>
     */
    private function prepareRequestParams(string $cityRef): array
    {
        $params = [
            'modelName' => 'Address',
            'calledMethod' => 'getStreet',
            "methodProperties" => [
                "CityRef" => $cityRef
            ]
        ];
        return $params;
    }

    /**
     * @param string $cityRef
     * @return \Perspective\NovaposhtaCatalog\Model\ResourceModel\Street\Street\Collection<StreetInterface>
     */
    private function getStreetsFromDb(string $cityRef): \Perspective\NovaposhtaCatalog\Model\ResourceModel\Street\Street\Collection
    {
        /** @var \Perspective\NovaposhtaCatalog\Model\ResourceModel\Street\Street\Collection $collection */
        $collection = $this->streetCollectionFactory->create();
        return $collection->addCityRefToFilter($cityRef);
    }

    /**
     * @param StreetInterface $street
     * @param int|string|null $streetId
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     */
    private function saveStreet(StreetInterface $street, $streetId = null): void
    {
        if ($streetId !== null) {
            $street->setStreetId((int)$streetId);
        }
        try {
            //TODO: replace with repository
            $this->streetResource->save($street); // @phpstan-ignore-line
        } catch (\Exception $exception) {
            $this->log($exception->getMessage());
        }
    }

    /**
     * @param StreetInterface $street
     * @param StreetInterface $streetFromNP
     * @return bool
     */
    private function isStreetChanged(StreetInterface $street, StreetInterface $streetFromNP): bool
    {
        return ($street->getRef() !== $streetFromNP->getRef()) ||
            ($street->getDescription() !== $streetFromNP->getDescription()) ||
            ($street->getStreetTypeRef() !== $streetFromNP->getStreetTypeRef()) ||
            ($street->getStreetType() !== $streetFromNP->getStreetType());
    }

    /**
     * @param string|\Magento\Framework\Phrase $message
     */
    private function log($message): void
    {
        if (is_callable($this->logger)) {
            ($this->logger)($message);
        }
    }
}
