<?php


namespace Perspective\NovaposhtaCatalog\Model\Warehouse;

use Perspective\NovaposhtaCatalog\Api\Data\WarehouseTypesInterface;

class WarehouseTypes extends \Magento\Framework\Model\AbstractExtensibleModel implements WarehouseTypesInterface
{

    protected function _construct()
    {
        $this->_init(\Perspective\NovaposhtaCatalog\Model\ResourceModel\Warehouse\Warehouse::class);
    }

    public function setDescriptionUa($data)
    {
        return $this->setData(self::DESCRIPTION_UA, $data);
    }

    public function setDescriptionRu($data)
    {
        return $this->setData(self::DESCRIPTION_RU, $data);
    }

    public function setRef($data)
    {
        return $this->setData(self::REF, $data);
    }

    public function getDescriptionUa()
    {
        return $this->getData(self::DESCRIPTION_UA);
    }

    public function getDescriptionRu()
    {
        return $this->getData(self::DESCRIPTION_RU);
    }

    public function getRef()
    {
        return $this->getData(self::REF);
    }
}
