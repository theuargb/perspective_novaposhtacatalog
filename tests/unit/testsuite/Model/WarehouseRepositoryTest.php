<?php


namespace Perspective\NovaposhtaCatalog\tests\unit\testsuite\Model;

use Perspective\NovaposhtaCatalog\Model\ResourceModel\Warehouse\Warehouse;
use Perspective\NovaposhtaCatalog\Model\ResourceModel\Warehouse\Warehouse\Collection;
use Perspective\NovaposhtaCatalog\Model\Warehouse\Warehouse as WarehouseModelRealObj;
use Perspective\NovaposhtaCatalog\Model\Warehouse\WarehouseFactory;
use Perspective\NovaposhtaCatalog\Model\WarehouseRepository as TestClass;
use Perspective\NovaposhtaCatalog\tests\unit\TestHelpers\FactoryMockHelper;

class WarehouseRepositoryTest extends \PHPUnit\Framework\TestCase
{
    public $objMan;
    /**
     * @var TestClass
     */
    public $testClass;
    /**
     * @var \Magento\Framework\App\ObjectManager
     */
    public $objManMage;
    /**
     * @var \Perspective\NovaposhtaCatalog\tests\unit\TestHelpers\MageObjMan
     */
    public $realObjMan;
    public $warehouseResourceModelCollectionRealObj;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $warehouseResourceModelCollectionFactoryMock;
    public $warehouseResourceModelRealObj;
    public $warehouseResourceModelFactoryMockReturnReal;
    public $warehouseModelRealObj;

    public function setUp()
    {
        $this->objMan = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);
        $this->realObjMan = new \Perspective\NovaposhtaCatalog\tests\unit\TestHelpers\MageObjMan();
        $this->warehouseResourceModelCollectionRealObj = $this->realObjMan->objectManager
            ->create(Collection::class);
        $this->warehouseResourceModelCollectionFactoryMock = $this
            ->getMockBuilder(
                \Perspective\NovaposhtaCatalog\Model\ResourceModel\Warehouse\Warehouse\CollectionFactory::class
            )
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();
        $this->warehouseResourceModelCollectionFactoryMock->method('create')
            ->willReturn($this->warehouseResourceModelCollectionRealObj);
        $this->warehouseResourceModelFactoryMockReturnReal = $this->getMockBuilder(WarehouseFactory::class)
            ->setMethods(['create'])->disableOriginalConstructor()->getMock();
        $this->warehouseModelRealObj = $this->realObjMan->objectManager->create(WarehouseModelRealObj::class);
        $this->warehouseResourceModelFactoryMockReturnReal->method('create')->willReturn($this->warehouseModelRealObj);
        $this->warehouseResourceModelRealObj = $this->realObjMan->objectManager->create(Warehouse::class);
        $this->testClass = $this->objMan->getObject(TestClass::class, [
            'warehouseCollectionFactory' => $this->warehouseResourceModelCollectionFactoryMock,
            'warehouseResourceModel' => $this->warehouseResourceModelRealObj,
            'warehouseModelFactory' => $this->warehouseResourceModelFactoryMockReturnReal,
        ]);
    }

    public function testGetWarehouseById()
    {
        $item = $this->testClass->getWarehouseById(92);
        $this->assertNotEmpty($item->getRef());
        $this->assertEquals('Шостка', $item->getCityDescriptionUa());
    }

    public function testGetListOfWarehousesByCityRef()
    {
        $items = $this->testClass->getListOfWarehousesByCityRef('e221d64c-391c-11dd-90d9-001a92567626', 'uk_Ua');
        $this->assertArrayHasKey('value', $items[0]);
        $this->assertArrayHasKey('label', $items[0]);
        $this->assertNotEmpty($items[0]['value']);
        $this->assertEquals($items[0]['value'], '92');
    }
}
