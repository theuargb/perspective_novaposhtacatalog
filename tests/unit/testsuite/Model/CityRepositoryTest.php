<?php


namespace Perspective\NovaposhtaCatalog\tests\unit\testsuite\Model;

use Perspective\NovaposhtaCatalog\Model\City\City;
use Perspective\NovaposhtaCatalog\Model\City\CityFactory;
use Perspective\NovaposhtaCatalog\Model\CityRepository as TestClass;
use Perspective\NovaposhtaCatalog\Model\ResourceModel\City\City as CityResourceModel;
use Perspective\NovaposhtaCatalog\Model\ResourceModel\City\City\Collection;
use Perspective\NovaposhtaCatalog\Model\ResourceModel\City\City\CollectionFactory;
use Perspective\NovaposhtaCatalog\tests\unit\TestHelpers\FactoryMockHelper;

/**
 * Class CityRepositoryTest
 * Test for city repository
 */
class CityRepositoryTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var \Magento\Framework\TestFramework\Unit\Helper\ObjectManager
     */
    public $objMan;
    /**
     * @var TestClass
     */
    public $testClass;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $cityFactory;
    /**
     * @var \Perspective\NovaposhtaCatalog\tests\unit\TestHelpers\FactoryHelper
     */
    public $factoryHelper;
    /**
     * @var \Perspective\NovaposhtaCatalog\tests\unit\testsuite\Model\FactoryMockHelper
     */
    public $cityResourceModelCollectionFactory;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $cityResourceModelCollection;
    /**
     * @var \Perspective\NovaposhtaCatalog\tests\unit\TestHelpers\FactoryHelper
     */
    public $factoryMockHelper;
    /**
     * @var \Magento\Framework\App\ObjectManager
     */
    public $objManMage;
    /**
     * @var \Perspective\NovaposhtaCatalog\tests\unit\TestHelpers\MageObjMan
     */
    public $realObjMan;
    /**
     * @var mixed
     */
    public $cityResourceModelCollectionRealObj;
    /**
     * @var \PHPUnit_Framework_MockObject_MockBuilder
     */
    public $cityResourceModelCollectionFactoryMock;
    public $cityRealModel;
    /**
     * @var \PHPUnit_Framework_MockObject_Builder_InvocationMocker
     */
    public $cityResourceModelReal;
    public $cityId = 71;

    /**
     * @throws \Exception
     */
    public function setUp()
    {
        $this->objMan = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);
        $this->factoryHelper = new \Perspective\NovaposhtaCatalog\tests\unit\TestHelpers\FactoryHelper();
        $this->factoryMockHelper = new \Perspective\NovaposhtaCatalog\tests\unit\TestHelpers\FactoryMockHelper();
        $this->realObjMan = new \Perspective\NovaposhtaCatalog\tests\unit\TestHelpers\MageObjMan();
        $this->cityRealModel = $this->realObjMan->objectManager->create(City::class);// $this->factoryHelper->getMockupFactory(City::class);
        $this->cityResourceModelCollection = $this->realObjMan->objectManager->create(Collection::class);
        $this->cityFactory = $this->getMockBuilder(CityFactory::class)
            ->setMethods(['create'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->cityResourceModelReal = $this->realObjMan->objectManager->create(CityResourceModel::class);
        $this->cityFactory->method('create')->willReturn($this->cityRealModel);
        $this->cityResourceModelCollectionFactory = $this->factoryMockHelper->getMockupFactory(Collection::class);
        $this->cityResourceModelCollectionFactory->method('create')->willReturn($this->cityResourceModelCollection);
        $this->cityResourceModelCollectionRealObj = $this->realObjMan->objectManager->create(Collection::class);
        $this->cityResourceModelCollectionFactoryMock = $this->getMockBuilder(CollectionFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();
        $this->cityResourceModelCollectionFactoryMock->method('create')
            ->willReturn($this->cityResourceModelCollectionRealObj);
        $this->testClass = $this->objMan->getObject(TestClass::class, [
            'cityFactory' => $this->cityFactory,
            'collectionFactory' => $this->cityResourceModelCollectionFactoryMock,
            'cityResourceModel' => $this->cityResourceModelReal
        ]);
    }

    /**
     * Gets test for given id
     */
    public function testGetCityById()
    {
        $item = $this->testClass->getCityById($this->cityId);
        $this->assertNotEmpty($item->getRef());
        $this->assertEquals(intval($item->getCityID()), $this->cityId);
        $this->assertEquals($item->getDescriptionUa(), 'Шостка');
    }

    /**
     * Gets test for given name
     */
    public function testGetCityByName()
    {
        $items = $this->testClass->getCityByName('Шостка');
        $indexedItems = array_values($items);
        $this->assertEquals('Шостка', $indexedItems[0]->getDescriptionUa());
    }

    /**
     * Gets test for all city
     */
    public function testGetAllCity()
    {
        $cityId = 71;
        $preparedArr = $this->testClass->getAllCity('uk_Ua');
        $this->assertNotEmpty($preparedArr);
        foreach ($preparedArr as $idx => $item) {
            if ($item['label'] === 'Шостка') {
                $this->assertNotEmpty($item['value']);
                $cityId = $item['value'];
                $cityModel = $this->testClass->getCityById(71);
                $this->assertEquals(intval($cityModel->getId()), $idx + 1);
            }
        }
    }
}
