<?php


namespace Perspective\NovaposhtaCatalog\tests\unit\testsuite\Controller;

use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\HTTP\ZendClient;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Perspective\NovaposhtaCatalog\Controller\Adminhtml\Sync\City as TestClass;
use Perspective\NovaposhtaCatalog\Helper\CronSyncDateLastUpdate;
use Perspective\NovaposhtaCatalog\Model\City\City;
use Perspective\NovaposhtaCatalog\Model\ResourceModel\City\City\Collection;
use Perspective\NovaposhtaCatalog\tests\unit\TestHelpers\ApiHelper;
use Perspective\NovaposhtaCatalog\tests\unit\TestHelpers\CollectionHelper;

/**
 * Class CityTest
 * Test for city sync
 */
class CityTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var TestClass
     */
    public $testClass;
    /**
     * @var \Magento\Framework\TestFramework\Unit\Helper\ObjectManager
     */
    public $objMan;

    /**
     * @var
     */
    public $zendClientFactory;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $collectionFactory;

    /**
     * @var
     */
    public $cityFactory;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $configHelperMock;
    /**
     * @var \Perspective\NovaposhtaCatalog\tests\unit\TestHelpers\FactoryHelper
     */
    public $factoryHelper;
    /**
     * @var object
     */
    public $jsonSerializer;
    /**
     * @var object
     */
    public $abstractCollection;
    /**
     * @var \Perspective\NovaposhtaCatalog\tests\unit\TestHelpers\CollectionHelper
     */
    public $collectionHelper;
    /**
     * @var void
     */
    public $dbCollection;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $connectionMock;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $selectMock;
    /**
     * @var \Perspective\NovaposhtaCatalog\tests\unit\TestHelpers\MageObjMan
     */
    public $realObjMan;

    /**
     *
     */
    public function setUp()
    {
        $this->factoryHelper = new \Perspective\NovaposhtaCatalog\tests\unit\TestHelpers\FactoryHelper();
        $this->objMan = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);
        $this->realObjMan = new \Perspective\NovaposhtaCatalog\tests\unit\TestHelpers\MageObjMan();
        $this->configHelperMock = $this->getMockBuilder(\Perspective\NovaposhtaCatalog\Helper\Config::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->collectionFactory = $this
            ->getMockBuilder(\Perspective\NovaposhtaCatalog\Model\ResourceModel\City\City\CollectionFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();
        $this->connectionMock = $this->createMock(\Magento\Framework\DB\Adapter\Pdo\Mysql::class);
        $this->selectMock = $this->createMock(\Magento\Framework\DB\Select::class);
        $this->jsonSerializer = $this->objMan->getObject(\Magento\Framework\Serialize\Serializer\Json::class);
        $this->configHelperMock->method('getIsEnabledConfig')->willReturn(1);
        $this->configHelperMock->method('getApiKeyConfig')->willReturn(ApiHelper::API_KEY);
        $this->collectionHelper = new \Perspective\NovaposhtaCatalog\tests\unit\TestHelpers\CollectionHelper();
        //  $this->dbCollection = $this->collectionHelper->prepareUserCollection(Collection::class);
        $this->dbCollection = $this->realObjMan->objectManager->create(Collection::class);
        $this->collectionFactory->method('create')->willReturn($this->dbCollection);

        $this->testClass = $this->objMan->getObject(
            TestClass::class,
            [
                'configHelper' => $this->configHelperMock,
                'httpClientFactory' => $this->factoryHelper->getMockupFactory(ZendClient::class),
                'cityFactory' => $this->factoryHelper->getMockupFactory(City::class),
                'cityResourceModelCollectionFactory' => $this->collectionFactory,
                'jsonSerializer' => $this->jsonSerializer,
                'resultJsonFactory' => $this->factoryHelper->getMockupFactory(Json::class),
                'connection' => $this->dbCollection,

            ]
        );
    }

    /**
     * @throws \Magento\Framework\Exception\NotFoundException
     * @throws \ReflectionException
     */
    public function testExecute()
    {
        $res = $this->testClass->execute();
        $reflectionClass = new \ReflectionClass(get_class($res));
        $p = $reflectionClass->getProperty('json');
        $p->setAccessible(true);
        $resJson = $p->getValue($res);
        $jsonObj = json_decode($resJson);
        $this->assertEquals($jsonObj->error, false);
    }
}
