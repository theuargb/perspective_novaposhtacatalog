<?php


namespace Perspective\NovaposhtaCatalog\tests\unit\testsuite\Block\System\Config;

use Perspective\NovaposhtaCatalog\Block\System\Config\Collect as TestClass;

class CollectTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var TestClass
     */
    private $testClass;
    /**
     * @var \Magento\Framework\TestFramework\Unit\Helper\ObjectManager
     */
    public $objMan;
    /**
     * @var void
     */
    public $testClassMock;

    public function setUp()
    {
        $this->objMan = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);
        $this->testClass = $this->objMan->getObject(TestClass::class);
    }

    public function testGetButtonTemplate()
    {
        $exp = 'Perspective_NovaposhtaCatalog::system/config/collect.phtml';
        $actual = $this->testClass->getButtonTemplate();
        $this->assertEquals($exp, $actual);
    }

    public function testSetButtonTemplate()
    {
        $exp = 'Perspective_NovaposhtaCatalog::system/config/test.phtml';
        $this->testClass->setButtonTemplate('Perspective_NovaposhtaCatalog::system/config/test.phtml');
        $actual = $this->testClass->getButtonTemplate();
        $this->assertEquals($exp, $actual);
    }

    public function testGetTranstlitedName()
    {
        $exp = 'Button';
        $mock = $this->createMock(TestClass::class);
        $mock->method('getTranstlitedName')->willReturn('Button');
        $res = $mock->getTranstlitedName();
        $this->assertEquals($exp, $res);
    }

    public function testGetButtonName()
    {
        $exp = 'Button';
        $actual = $this->testClass->getButtonName();
        $this->assertEquals($exp, $actual);
    }
}
